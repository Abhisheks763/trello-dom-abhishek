import { updateCheckItem, deleteCheckItem } from "./API-boardDisplay.js";

let token = "44e8473a750e312013a8fbb00410e643625fb141b62844aedc9623108a63a23a";
let key = "effc3c79838133e467e118a4abdf3292";

function createCheckItemsUi(checklistId, checkItemId, checkItemName, state) {
  let ChecklistDiv = document.querySelector(
    `[data-checklistid="${checklistId}"]`
  );

  let checked;
  if (state === "complete") {
    checked = true;
  } else {
    checked = false;
  }
  console.log(state, checked);
  let checkItemDiv = document.createElement("div");
  checkItemDiv.innerHTML = `<div><input type =checkbox  id=${checkItemId}><label for =${checkItemId}>${checkItemName}</label></div>`;
  checkItemDiv.firstElementChild.firstElementChild.checked = checked;
  checkItemDiv.setAttribute("data-checkItemId", checkItemId);
  checkItemDiv.classList.add("checkItem");

  //delete checkitem btn
  let delCheckItemBtn = document.createElement("div");
  delCheckItemBtn.classList.add("delete-checkitem");
  delCheckItemBtn.innerHTML = '<i class="fas fa-trash"></i>';

  //event listner
  delCheckItemBtn.addEventListener("click", () => {
    deleteCheckItem(checklistId, checkItemId, key, token);
    document.querySelector(`[data-checkitemid='${checkItemId}']`).remove();
  });
  checkItemDiv.appendChild(delCheckItemBtn);

  checkItemDiv.addEventListener("click", () => {
    let cardId = document
      .getElementById("card-details")
      .getAttribute("data-cardid-carddetailsdiv");
    updateCheckItem(cardId, checklistId, checkItemId, key, token);
  });
  ChecklistDiv.insertBefore(checkItemDiv, ChecklistDiv.lastElementChild);
}

export { createCheckItemsUi };
