import { deleteCard } from "./API-boardDisplay.js";
import { cardDetailWindow } from "./card-detail-window.js";
let token = "44e8473a750e312013a8fbb00410e643625fb141b62844aedc9623108a63a23a";
let key = "effc3c79838133e467e118a4abdf3292";

function createCardUi(name, id) {
  let cardContainer = document.querySelector("#all-cards");

  let newCard = document.createElement("div");
  let deleteBtn = document.createElement("div");

  deleteBtn.classList.add("delete-card");
  newCard.classList.add("card-main-body");

  newCard.innerText = name;
  deleteBtn.innerHTML = '<i class="fas fa-trash"></i>';

  newCard.setAttribute("data-cardId", `${id}`);
  deleteBtn.setAttribute("data-cardId-del", `${id}`);
  deleteBtn.setAttribute("onclick", "event.stopPropagation()");

  newCard.appendChild(deleteBtn);
  cardContainer.appendChild(newCard);

  //card delete Btn
  let deleteCardBtn = document.querySelector(`[data-cardId-del="${id}"]`);

  deleteCardBtn.addEventListener("click", () => {
    //remove card from server
    let cardIDToDel = deleteCardBtn.getAttribute("data-cardId-del");
    deleteCard(cardIDToDel, key, token);
    //remove card from UI

    document.querySelector(`[data-cardid="${cardIDToDel}"]`).remove();
  });

  //Display card Details
  newCard.addEventListener("click", () => {
    cardDetailWindow(id, name);
  });
}
export { createCardUi };
