import {
  deleteChecklist,
  getCheckItems,
  createCheckItem,
} from "./API-boardDisplay.js";
import { createCheckItemsUi } from "./createCheckItemsUi.js";
import { messages } from "./messages.js";
let token = "44e8473a750e312013a8fbb00410e643625fb141b62844aedc9623108a63a23a";
let key = "effc3c79838133e467e118a4abdf3292";

function createChecklistUi(checklistId, checklistName) {
  let checklistDiv = document.createElement("div");
  let titleDiv = document.createElement("div");
  titleDiv.classList.add("checklist-title");
  checklistDiv.classList.add("checklist-div");
  checklistDiv.setAttribute("data-checklistId", checklistId);

  let checklistNameDiv = document.createElement("div");
  checklistNameDiv.innerHTML = `<i class="far fa-check-square"></i><p>${checklistName}</p>`;

  let checklistDelBtn = document.createElement("div");
  checklistDelBtn.innerText = "Delete";
  checklistDelBtn.setAttribute("data-checklistId-del", checklistId);
  //delete checklists
  checklistDelBtn.addEventListener("click", () => {
    let cardId = document
      .querySelector("#card-details")
      .getAttribute("data-cardid-carddetailsdiv");

    deleteChecklist(cardId, checklistId, key, token);
    let reqChecklist = document.querySelector(
      `[data-checklistid="${checklistId}"]`
    );
    reqChecklist.remove();
  });

  //progress bar
  let progressBarDiv = document.createElement("div");
  progressBarDiv.classList.add("progress-div");
  let percent = document.createElement("span");
  percent.classList.add("percent");
  let progressBarOuter = document.createElement("div");
  progressBarOuter.classList.add("progress-bar-outer");
  let progressBarInner = document.createElement("div");
  progressBarInner.classList.add("progress-bar-inner");

  titleDiv.appendChild(checklistNameDiv);
  titleDiv.appendChild(checklistDelBtn);
  checklistDiv.appendChild(titleDiv);

  progressBarOuter.appendChild(progressBarInner);
  document.querySelector(".checklist-collection").appendChild(checklistDiv);
  checklistDiv.appendChild(progressBarOuter);

  //create new checkItem btn
  let addCheckItemBtn = document.createElement("button");
  addCheckItemBtn.innerText = "Add item";
  addCheckItemBtn.addEventListener("click", () => {
    messages("Add new check item");
    document
      .querySelector(".messages-save")
      .addEventListener("click", async () => {
        let newCheckItemName = document.querySelector("#input").value;
        let data = await createCheckItem(
          checklistId,
          key,
          token,
          newCheckItemName
        );
        document.querySelector(".messages").remove();
        console.log(data);
        createCheckItemsUi(data.idChecklist, data.id, data.name);
      });
  });
  checklistDiv.insertBefore(
    addCheckItemBtn,
    checklistDiv.lastElementChild.nextSibling
  );
  //get all checkitems of this checklist
  (async () => {
    let allCheckitems = await getCheckItems(checklistId, key, token);
    allCheckitems.forEach((item) => {
      createCheckItemsUi(item.idChecklist, item.id, item.name, item.state);
    });
  })();
}
export { createChecklistUi };
