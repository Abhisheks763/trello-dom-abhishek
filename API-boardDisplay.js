function getList(boardId, key, token) {
  return fetch(
    `https://api.trello.com/1/boards/${boardId}/lists?key=${key}&token=${token}`,
    { method: "GET" }
  )
    .then((res) => {
      return res.json();
    })
    .then((res) => {
      return res[0];
    });
}

function getCards(listId, key, token) {
  return fetch(
    `https://api.trello.com/1/lists/${listId}/cards?key=${key}&token=${token}`,
    { method: "GET" }
  ).then((res) => {
    return res.json();
  });
}

function createCard(listId, key, token, name) {
  return fetch(
    `https://api.trello.com/1/cards?key=${key}&token=${token}&idList=${listId}&name=${name}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.text();
    })
    .then((text) => JSON.parse(text))
    .catch((err) => console.error(err));
}

function deleteCard(cardId, key, token) {
  return fetch(
    `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`,
    {
      method: "DELETE",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.text();
    })
    .then((text) => JSON.parse(text))
    .catch((err) => console.error(err));
}

function createChecklist(cardId, checklistName, key, token) {
  return fetch(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${token}&name=${checklistName}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.text();
    })
    .then((text) => JSON.parse(text))
    .catch((err) => console.error(err));
}

function getChecklist(cardId, key, token) {
  return fetch(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${token}`,
    {
      method: "GET",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.text();
    })
    .then((text) => JSON.parse(text))
    .catch((err) => console.error(err));
}

function deleteChecklist(cardId, checklistId, key, token) {
  return fetch(
    `https://api.trello.com/1/cards/${cardId}/checklists/${checklistId}?key=${key}&token=${token}`,
    {
      method: "DELETE",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.text();
    })
    .then((text) => console.log(text))
    .catch((err) => console.error(err));
}

function getCheckItems(checklistId, key, token) {
  console.log(checklistId, key, token);
  return fetch(
    `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${key}&token=${token}`,
    {
      method: "GET",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.text();
    })
    .then((text) => JSON.parse(text))
    .catch((err) => console.error(err));
}

function createCheckItem(checklistId, key, token, name) {
  return fetch(
    `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${key}&token=${token}&name=${name}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.text();
    })
    .then((text) => JSON.parse(text))
    .catch((err) => console.error(err));
}

function updateCheckItem(cardId, checklistId, checkItemId, key, token) {
  let state;
  let checkItem = document.getElementById(`${checkItemId}`);
  if (checkItem.checked) {
    state = "complete";
  } else {
    state = "incomplete";
  }

  return fetch(
    `https://api.trello.com/1/cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}?key=${key}&token=${token}&state=${state}`,
    {
      method: "PUT",
      headers: {
        Accept: "application/json",
      },
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.text();
    })
    .then((text) => console.log("update", text))
    .catch((err) => console.error(err));
}

function deleteCheckItem(checklistId, checkItemId, key, token) {
  return fetch(
    `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=${key}&token=${token}`,
    {
      method: "DELETE",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.text();
    })
    .then((text) => console.log(text));
}

export {
  getList,
  getCards,
  createCard,
  deleteCard,
  createChecklist,
  getChecklist,
  deleteChecklist,
  getCheckItems,
  createCheckItem,
  updateCheckItem,
  deleteCheckItem,
};
