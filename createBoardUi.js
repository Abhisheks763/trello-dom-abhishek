function createBoardUi(id, name) {
  let allBoards = document.getElementById("all-boards");

  let board = document.createElement("div");
  let a = document.createElement("a");
  let deleteBtn = document.createElement("div");

  board.classList.add("board");
  board.id = id;
  board.innerText = name;
  a.setAttribute("href", `Trello-Display.html?id=${id}&name=${name}`);

  a.appendChild(board);
  allBoards.insertBefore(a, allBoards.lastElementChild);
}
export { createBoardUi };
