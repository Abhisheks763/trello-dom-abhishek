import {
  getCards,
  getList,
  createCard,
  deleteCard,
} from "./API-boardDisplay.js";
import { createCardUi } from "./createCardUi.js";
import { messages } from "./messages.js";
let token = "44e8473a750e312013a8fbb00410e643625fb141b62844aedc9623108a63a23a";
let key = "effc3c79838133e467e118a4abdf3292";

//Getting board name and id
let urlPath = window.location.href;
let newUrlPath = new URL(urlPath);
let boardId = newUrlPath.searchParams.get("id");
let boardName = newUrlPath.searchParams.get("name");

//Updating board name in HTML page
document.querySelector("#board-name").innerText = boardName;

//get all lists
document.addEventListener("DOMContentLoaded", async function () {
  let reqList = await getList(boardId, key, token);

  let displayedList = document.querySelector("#list");
  displayedList.setAttribute("data-listId", reqList.id);
  document.querySelector("#list-title").innerText = reqList.name;

  //get all cards from server
  let allCards = await getCards(reqList.id, key, token);

  //Displaying all cards on UI
  for (let card of allCards) {
    createCardUi(card.name, card.id);
  }
});

// Create Card Btn

let addCardBtn = document.querySelector("#add-card");
addCardBtn.addEventListener("click", () => {
  messages("Add new card");
  let submitBtn = document.querySelector(".messages-save");
  submitBtn.addEventListener("click", async () => {
    let listId = document.querySelector("#list").getAttribute("data-listId");
    let newCardName = document.querySelector("#input").value;
    let newCard = await createCard(listId, key, token, newCardName);

    //remove message pop up
    document.querySelector(".messages").remove();
    createCardUi(newCard.name, newCard.id);
  });
});
