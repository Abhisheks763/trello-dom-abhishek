import { getChecklist, createChecklist } from "./API-boardDisplay.js";
import { createChecklistUi } from "./createChecklistUi.js";
import { messages } from "./messages.js";

let token = "44e8473a750e312013a8fbb00410e643625fb141b62844aedc9623108a63a23a";
let key = "effc3c79838133e467e118a4abdf3292";

function cardDetailWindow(cardId, cardName) {
  let cardDetailsWindow = document.getElementById("card-details-window");
  cardDetailsWindow.style.display = "flex";

  //show card details
  let cardDetailsDiv = document.getElementById("card-details");
  cardDetailsDiv.setAttribute("data-cardId-cardDetailsDiv", cardId);

  let cardNameDiv = document.getElementById("card-name");
  cardNameDiv.innerHTML = `<h1>${cardName}</h1>`;

  //create checklist container
  let checklistCollection = document.createElement("div");
  checklistCollection.classList.add("checklist-collection");
  cardDetailsDiv.appendChild(checklistCollection);
  //   show checklists
  getChecklist(cardId, key, token).then((res) => {
    for (let checklist of res) {
      createChecklistUi(checklist.id, checklist.name);
    }
  });

  //add checklist btn
  let createChecklistBtn = document.querySelector("#create-checklist");
  createChecklistBtn.addEventListener("click", msgChecklist);
  function msgChecklist() {
    messages("Create new checklist");
    document
      .querySelector(".messages-save")
      .addEventListener("click", async () => {
        let newChecklistName = document.querySelector("#input").value;
        let data = await createChecklist(cardId, newChecklistName, key, token);
        document.querySelector(".messages").remove();
        createChecklistUi(data.id, data.name);
      });
  }

  //cardDetailWindow event listener
  cardDetailsWindow.addEventListener("click", showWindow);
  function showWindow(event) {
    if (event.target == cardDetailsWindow) {
      document.querySelector(".checklist-collection").remove();
      cardDetailsWindow.style.display = "none";
      //   removeEventListeners on closing card detail window
      cardDetailsWindow.removeEventListener("click", showWindow);
      createChecklistBtn.removeEventListener("click", msgChecklist);
    }
  }
}

export { cardDetailWindow };
