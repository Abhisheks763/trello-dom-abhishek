function getAllBoards(key, token) {
  return fetch(
    `https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`
  )
    .then((res) => res.text())
    .then((res) => {
      return JSON.parse(res);
    });
}

function createBoard(name, key, token) {
  return fetch(
    `https://api.trello.com/1/boards/?key=${key}&token=${token}&name=${name}`,
    {
      method: "POST",
    }
  ).then((res) => {
    return res.json();
  });
}
export { getAllBoards, createBoard };
