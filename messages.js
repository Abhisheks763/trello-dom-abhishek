function messages(title) {
  const form = document.createElement("form");

  const inputDiv = document.createElement("div");

  const btnDiv = document.createElement("div");

  inputDiv.innerHTML = `<label for="input">${title} : </label>
      <input  id="input"  placeholder="Enter ${title} here...">`;
  btnDiv.innerHTML = `<div class="messages-save">Save</div>
      <div class="messages-cancel">Cancel</div>`;
  btnDiv.classList.add("messages-btn");
  form.append(inputDiv);
  form.append(btnDiv);

  const messagesDiv = document.createElement("div");
  messagesDiv.classList.add("board");
  messagesDiv.classList.add("messages");

  messagesDiv.append(form);
  document.body.append(messagesDiv);

  btnDiv.lastElementChild.addEventListener("click", () => {
    messagesDiv.remove();
  });
}

export { messages };
