let token = "44e8473a750e312013a8fbb00410e643625fb141b62844aedc9623108a63a23a";
let key = "effc3c79838133e467e118a4abdf3292";

import { createBoard, getAllBoards } from "./API-boards.js";
import { createBoardUi } from "./createBoardUi.js";
import { messages } from "./messages.js";

//Display all boards on UI
document.addEventListener("DOMContentLoaded", async () => {
  let allBoardsData = await getAllBoards(key, token);

  for (let board of allBoardsData) {
    createBoardUi(board.id, board.name);
  }
});
//Add new board btn

let newBoardBtn = document.createElement("div");
newBoardBtn.classList.add("board");
newBoardBtn.innerHTML = '<i class="fas fa-plus"></i>';
document
  .querySelector("#all-boards")
  .insertBefore(newBoardBtn, newBoardBtn.lastElementChild.nextSibling);

newBoardBtn.addEventListener("click", () => {
  messages("Name of new board");
  document
    .querySelector(".messages-save")
    .addEventListener("click", async () => {
      let newBoardName = document.querySelector("#input").value;
      let data = await createBoard(newBoardName, key, token);
      createBoardUi(data.id, data.name);
      document.querySelector(".messages").remove();
    });
});
